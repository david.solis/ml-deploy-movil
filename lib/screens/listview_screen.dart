import 'package:flutter/material.dart';
import 'package:flutterappml_2/router/app_routes.dart';
import 'package:flutterappml_2/screens/screens.dart';

class ListviewScreen extends StatelessWidget {
  /*final options = const [
    "Configuración",
    "Acerca de",
    "Mi cuenta",
    "Mi cuenta",
    "Mi cuenta",
    "Mi cuenta"
  ];*/
  const ListviewScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          actions: [
            Container(
                margin: EdgeInsets.only(right: 8),
                child: CircleAvatar(
                    child: Text('DS'), backgroundColor: Colors.blue[900]))
          ],
          title: Text("Listview"),
          backgroundColor: Colors.indigo,
        ),
        drawer: Drawer(
            child: ListView(children: const [
          DrawerHeader(
              child: Text('Menu'),
              decoration: BoxDecoration(color: Colors.amber)),
          ListTile(
              leading: Icon(Icons.account_circle, color: Colors.amber),
              title: Text('Profile')),
          ListTile(
              leading: Icon(Icons.account_circle, color: Colors.amber),
              title: Text('Profile')),
          ListTile(
              leading: Icon(Icons.account_circle, color: Colors.amber),
              title: Text('Profile')),
          ListTile(
              leading: Icon(Icons.account_circle, color: Colors.amber),
              title: Text('Profile')),
        ])),
        body: ListView.separated(
          itemCount: AppRoutes.options.length,
          separatorBuilder: (context, index) => const Divider(),
          itemBuilder: (context, index) => ListTile(
            title: Text('${AppRoutes.options[index].name}'),
            trailing: Icon(AppRoutes.options[index].icon, color: Colors.indigo),
            onTap: () {
              Navigator.pushNamed(context, AppRoutes.options[index].route);
              //print(options[index]);
            },
          ),
        ));

    // ...options.map((option) => ListTile(
    //       title: Text(option), trailing: const Icon(Icons.settings))),
  }
}
