import 'package:flutter/material.dart';

import '../widgets/custom_textformfield.dart';

class FormScreen extends StatelessWidget {
  const FormScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();
    final Map<String, String> form_values = {
      'firstname': 'David',
      'lastname': 'Solis',
      'work': 'Geeks',
      'email': 'prueba@gmail.com',
      'password': '12345',
      'role': 'admin'
    };

    return Scaffold(
        appBar: AppBar(title: Text('Formulario')),
        body: SingleChildScrollView(
          child: Form(
            key: formKey,
            child: Container(
              margin: EdgeInsets.all(16),
              child: Column(
                children: [
                  CustomTextformfield(
                    labelText: 'Nombres',
                    icon: Icons.person_outline,
                    formName: 'firstname',
                    formValues: form_values,
                  ),
                  CustomTextformfield(
                    labelText: 'Apellidos',
                    icon: Icons.person_outline,
                    formName: 'lastname',
                    formValues: form_values,
                  ),
                  CustomTextformfield(
                    labelText: 'Empresa',
                    icon: Icons.work,
                    formName: 'work',
                    formValues: form_values,
                  ),
                  CustomTextformfield(
                      labelText: 'Email',
                      icon: Icons.email,
                      formName: 'email',
                      formValues: form_values,
                      keyboardType: TextInputType.emailAddress),
                  CustomTextformfield(
                    labelText: 'Password',
                    icon: Icons.password,
                    formName: 'password',
                    formValues: form_values,
                    obscureText: true,
                  ),
                  DropdownButtonFormField(
                    items: [
                      DropdownMenuItem(
                          value: 'admin', child: Text('Administrador')),
                      DropdownMenuItem(value: 'client', child: Text('Cliente')),
                      DropdownMenuItem(
                          value: 'supervisor', child: Text('Supervisor'))
                    ],
                    onChanged: (value) {
                      form_values['role'] = value.toString();
                    },
                  ),
                  CheckboxListTile(value: false, onChanged: (value) {}),
                  SizedBox(height: 30),
                  ElevatedButton(
                    child: Text('Guardar'),
                    onPressed: () {
                      if (!formKey.currentState!.validate()) {
                        return;
                      }
                      print(form_values);

                      // Enviar estos parametros a un API
                      // Mostrar un mensaje de respuesta
                      // Notificar a los widgets para reload data
                    },
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
