import 'package:flutter/material.dart';
import '../widgets/custom_card.dart';

class CardScreen extends StatelessWidget {
  const CardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        body: ListView(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          children: const [
            CustomCard(
                label: 'Avengers 1',
                url:
                    'https://as01.epimg.net/meristation/imagenes/2021/03/06/reportajes/1615060486_238091_1615096762_sumario_normal.jpg'),
            CustomCard(
                label: 'Avengers 2',
                url:
                    'https://i0.wp.com/www.dailycal.org/assets/uploads/2021/04/movies_marvel_courtesy.jpg?ssl=1'),
            CustomCard(
                label: 'Capitana Marvel',
                url:
                    'https://www.madametussauds.com/blackpool/media/txklyzes/captain-marvel.jpg?anchor=center&mode=crop&width=500&height=500'),
            CustomCard(
                url:
                    'https://assets.simpleviewinc.com/simpleview/image/fetch/c_limit,q_75,w_1200/https://columbus.simpleviewcrm.com/images/calendar/MarvelCharacterBlock-sm.png'),
          ],
        ));
  }
}
