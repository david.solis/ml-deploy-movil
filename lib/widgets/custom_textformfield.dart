import 'package:flutter/material.dart';

class CustomTextformfield extends StatelessWidget {
  final String? labelText;
  final IconData? icon;
  final TextInputType? keyboardType;
  final bool obscureText;

  final String formName;
  final Map<String, String> formValues;

  const CustomTextformfield({
    Key? key,
    this.labelText,
    this.icon,
    this.keyboardType,
    this.obscureText = false,
    required this.formName,
    required this.formValues,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: keyboardType,
      obscureText: obscureText,
      decoration: InputDecoration(
        icon: icon == null ? null : Icon(icon),
        labelText: labelText,
      ),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      initialValue: '',
      textCapitalization: TextCapitalization.words,
      validator: (value) {
        if (value == '') return 'Campo requerido';
        return value!.length <= 4 ? 'Minimo 4 caracteres' : null;
      },
      onChanged: (value) {
        //print('$value');
        formValues[formName] = value;
      },
    );
  }
}
