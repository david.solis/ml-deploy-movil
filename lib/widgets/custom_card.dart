import 'package:flutter/material.dart';

class CustomCard extends StatelessWidget {
  final String url;
  final String? label;

  const CustomCard({
    Key? key,
    required this.url,
    this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          FadeInImage(
              placeholder: AssetImage('assets/spinning-loading.gif'),
              image: NetworkImage(url)),
          if (label != null)
            Container(padding: EdgeInsets.all(8), child: Text(label!)),
          //Container(padding: EdgeInsets.all(8), child: Text(label ?? 'Default')),
          TextButton(
            child: Text('Like'),
            onPressed: () {},
          ),
          TextButton(
            child: Text('Edit'),
            onPressed: () {},
          )
        ],
      ),
    );
  }
}
