import 'package:flutter/material.dart';
import 'package:flutterappml_2/models/menu_option.dart';
import 'package:flutterappml_2/screens/form_screen.dart';
import 'package:flutterappml_2/screens/screens.dart';

class AppRoutes {
  static const initialRoute = 'listview';

  static final options = <MenuOption>[
    MenuOption(
        route: 'listview',
        icon: Icons.list,
        name: 'Listview',
        screen: const ListviewScreen()),
    MenuOption(
        route: 'alert',
        icon: Icons.list,
        name: 'Alert',
        screen: const AlertScreen()),
    MenuOption(
        route: 'card',
        icon: Icons.list,
        name: 'Card',
        screen: const CardScreen()),
    MenuOption(
        route: 'form',
        icon: Icons.list,
        name: 'Form',
        screen: const FormScreen()),
  ];

  static Map<String, Widget Function(BuildContext)> routes = {
    'listview': (BuildContext context) => ListviewScreen(),
    'alert': (BuildContext context) => AlertScreen(),
    'card': (BuildContext context) => CardScreen(),
    'form': (BuildContext context) => FormScreen(),
  };
}
