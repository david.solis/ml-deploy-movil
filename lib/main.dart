import 'package:flutter/material.dart';
import 'package:flutterappml_2/router/app_routes.dart';
import 'package:flutterappml_2/screens/screens.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: AppRoutes.routes,
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      //home: const ListviewScreen(),
      initialRoute: AppRoutes.initialRoute,
      onGenerateRoute: (settings) {
        print(settings);

        return MaterialPageRoute(
          builder: (context) => const CardScreen(),
        );
      },
    );
  }
}
